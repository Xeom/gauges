#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>

#define UART_BAUD 9600

#define PWM_A OCR2A
#define PWM_B OCR2B

volatile uint8_t c;

void pwm_init(void)
{
	// Set up timer output
	TCCR2A = _BV(COM2A1) | _BV(COM2B1);
	DDRB |= _BV(PB3);
	DDRD |= _BV(PD3);

	// Set up timer for PWM
	TCCR2A |= _BV(WGM20) | _BV(WGM21);
	TCCR2B = _BV(CS20);
}

void uart_init(void)
{
	// Set the baudrate
	UBRR0H = (F_CPU / UART_BAUD / 16 - 1) >> 8;
	UBRR0L = (F_CPU / UART_BAUD / 16 - 1)  & 0xff;

	// Set up other things
	UCSR0B = _BV(RXEN0) | _BV(RXCIE0) | _BV(UCSZ02);
	UCSR0C = _BV(UCSZ00) | _BV(UCSZ01);
}

ISR(USART_RX_vect)
{
	PWM_A = UDR0;
	PWM_B = UDR0;
}

uint8_t prev = 0;
int main()
{
	uart_init();
	pwm_init();
	ACSR |= _BV(ACD);
	PWM_A = 0;
	PWM_B = 0;
	sei();

	while (1) {
		sleep_mode();
	}

	return 0;
}
