CPU ?= atmega328p
CPU_HZ ?= 16000000
PORT ?= /dev/ttyUSB0

thing.hex: thing.elf
	avr-objcopy -O ihex $< $@

thing.elf: main.c
	avr-gcc -lm -mmcu=$(CPU) -DF_CPU=$(CPU_HZ) -Wpedantic -Wall -Wextra $< -o $@

.PHONY: upload
upload: thing.hex
	avrdude -v -p $(CPU) -c arduino -P $(PORT) -b 57600 -D -U flash:w:$<:i

.PHONY: clean
clean:
	rm -f thing.elf thing.hex
